# libtiff

This is a private libtiff repository forked from https://gitlab.com/libtiff/libtiff

## Notes

Build:

```sh
source ~/emsdk/emsdk_env.sh
emmake make tiff2pdf
```
